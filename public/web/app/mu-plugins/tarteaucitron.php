<?php

// securiser l'accès au fichier
if ( ! defined( 'ABSPATH' ) ) exit;


// Chargement TAC
add_action( 'wp_enqueue_scripts', 'webplus_tac_load' );
function webplus_tac_load() {
    wp_enqueue_script( 'tarteaucitron', 'https://cdn.jsdelivr.net/gh/AmauriC/tarteaucitron.js@1.8/tarteaucitron.min.js' );
}

// Initialisation TAC
add_action( 'wp_head', 'webplus_tac_init' );
function webplus_tac_init() {
    ?>

    <script type="text/javascript">
        tarteaucitron.init({
            "privacyUrl": "", /* Privacy policy url */
            "hashtag": "#tarteaucitron", /* Open the panel with this hashtag */
            "cookieName": "tartaucitron", /* Cookie name */
            "orientation": "bottom", /* Banner position (top - bottom) */
            "showAlertSmall": false, /* Show the small banner on bottom right */
            "cookieslist": true, /* Show the cookie list */
            "adblocker": false, /* Show a Warning if an adblocker is detected */
            "AcceptAllCta" : true, /* Show the accept all button when highPrivacy on */
            "DenyAllCta" : false, /* Show the deny all button */
            "highPrivacy": false, /* Disable auto consent */
            "handleBrowserDNTRequest": false, /* If Do Not Track == 1, accept all */
            "removeCredit": true, /* Remove credit link */
            "moreInfoLink": false, /* Show more info link */
            "useExternalCss": false, /* If false, the tarteaucitron.css file will be loaded */
            "readmoreLink": "https://www.cnil.fr/fr/cookies-les-outils-pour-les-maitrise",
            //"cookieDomain": ".my-multisite-domaine.fr", /* Shared cookie for subdomain */
            }),
        tarteaucitron.user.analyticsUa = 'UA-147179879-1';
        tarteaucitron.user.analyticsMore = function () { /* add here your optionnal ga.push() */ };
        (tarteaucitron.job = tarteaucitron.job || []).push('analytics');
    </script>

<?php }

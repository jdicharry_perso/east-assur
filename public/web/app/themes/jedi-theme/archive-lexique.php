<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Timber\Timber;

class ArchiveLexiqueController extends Controller
{
    public function handle()
    {
        $context = Timber::get_context();
        $context['title'] = 'Lexique';
        $args = [
            'post_type' => 'lexique',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'ASC'
        ];
        $context['posts'] = Timber::get_posts($args);

        return new TimberResponse('templates/lexique.twig', $context);
    }
}

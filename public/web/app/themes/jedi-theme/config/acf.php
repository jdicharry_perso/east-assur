<?php
use App\Http\Gutenberg;

return [
    'type_loading_fields' => 'JSON', // PHP|JSON
    'autoloading' => true,
    'fields' => get_stylesheet_directory() . '/acf-json-fields',
    'block_type' => [
        'Mod title text image' => [
            'name'            => 'mod-title-text-image',
            'title'           => __('Block titre, texte et image', 'jedi-theme'),
            'render_callback' => [Gutenberg::class, 'renderMod'],
            'category'        => 'flexibles',
            'icon'            => 'share-alt2',
            'supports'        => ['align' => false]
        ],
        'Mod title blocs pictos' => [
            'name'            => 'mod-title-blocs-pictos',
            'title'           => __('Bloc titre et blocs pictos', 'jedi-theme'),
            'render_callback' => [Gutenberg::class, 'renderMod'],
            'category'        => 'flexibles',
            'icon'            => 'share-alt2',
            'supports'        => ['align' => false]
        ],
        'Mod accordion' => [
            'name'            => 'mod-accordion',
            'title'           => __('Bloc accordéon', 'jedi-theme'),
            'render_callback' => [Gutenberg::class, 'renderMod'],
            'category'        => 'flexibles',
            'icon'            => 'share-alt2',
            'supports'        => ['align' => false]
        ],
    ],
    'options' => [
        'main' => [
            'page_title' => 'Theme settings',
            'menu_title' => 'Theme settings',
            'menu_slug' => 'theme-settings',
            'capability' => 'edit_posts',
            'redirect' => false
        ]
    ]
];

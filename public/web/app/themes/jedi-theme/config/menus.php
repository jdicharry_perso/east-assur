<?php

return [
    /**
     * List of menus to register with WordPress during bootstrap
     */
    'menus' => [
        'main-nav' => __('Menu principal'),
        'footer-nav' => __('Menu du footer'),
    ],
];

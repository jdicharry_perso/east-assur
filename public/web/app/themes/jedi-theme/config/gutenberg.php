<?php
/**
 * Disable gutenberg for templates
 */
return [
    'page_on_front' => true,
    'page_for_posts' => true,
    'templates' => [
        'page.php' ,
    ]
];

<?php

/**
 * The Template for the front-page
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Timber\Timber;

class FrontPageController extends Controller
{
    public function handle()
    {
        $context = Timber::get_context();
        $page = $context['posts'][0];

        $context['post'] = $page;
        $context['title'] = $page->title;
        $context['content'] = $page->content;
        $context['front_hero'] = $page->meta('front_hero');
        $context['front_cta_banner'] = $page->meta('front_cta_banner');
        $context['front_reassurance'] = $page->meta('front_reassurance');
        $context['front_image_cta'] = $page->meta('front_image_cta');
        $context['front_text_image'] = $page->meta('front_text_image');
        $context['front_pictos_text'] = $page->meta('front_pictos_text');
        $context['front_offers'] = $page->meta('front_offers');


        return new TimberResponse('templates/front-page.twig', $context);
    }
}

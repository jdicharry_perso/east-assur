<?php
/**
 * Template Name: Template Plan du site
 * The template for displaying Site Map pages.
 *.
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Timber\Timber;

class SitemapTemplateController extends Controller
{
    public function handle()
    {
        $context = Timber::get_context();
        $page = $context['posts'][0];

        $context['post'] = $page;
        $context['title'] = $page->title;
        $context['content'] = $page->content;
        $context['hero'] = $page->meta('hero');



        return new TimberResponse('templates/site-map.twig', $context);
    }
}

<?php

namespace App\PostTypes;

use Rareloop\Lumberjack\Post;

class Lexique extends Post
{
    /**
     * Return the key used to register the post type with WordPress
     * First parameter of the `register_post_type` function:
     * https://codex.wordpress.org/Function_Reference/register_post_type
     *
     * @return string
     */
    public static function getPostType()
    {
        return 'lexique';
    }

    /**
     * Return the config to use to register the post type with WordPress
     * Second parameter of the `register_post_type` function:
     * https://codex.wordpress.org/Function_Reference/register_post_type
     *
     * @return array
     */
    protected static function getPostTypeConfig()
    {
        return [
            'labels' => [
                'name' => __('Lexique'),
                'singular_name' => __('Lettre de l\'alphabet'),
            ],
            'public' => true,
            'menu_position'=> 5,
            'show_in_rest' => true,
            'has_archive' => true,
            'menu_icon' => 'dashicons-admin-page',
            'supports' => [
                'title',
                'editor',
            ],
            'rewrite' => [
                'slug' => __('lexique'),
            ],
        ];
    }

    // public function children($post_type = 'any', $childPostClass = false)
    // {
    //     $post_type = self::getPostType();
    //     return parent::children($post_type);
    // }
}

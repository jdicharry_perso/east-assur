<?php

namespace App\Http;

use Rareloop\Lumberjack\Http\Lumberjack as LumberjackCore;

class Lumberjack extends LumberjackCore
{
    public function addToContext($context)
    {
        $context['is_home'] = is_home();
        $context['is_front_page'] = is_front_page();
        $context['home'] = get_permalink( get_option( 'page_on_front' ) );
        if (get_page_by_path( 'data-projekt' )) {
            $context['credit_page_ID'] = get_the_permalink(get_page_by_path( 'data-projekt' )->ID);
        }


        // Ajout du picto pour la navigation fixe "Devis en ligne"
        $context['options'] = get_fields('options');
        foreach ($context['options']['fixed_nav_menu'] as $key => $page) {
            $context['options']['fixed_nav_menu'][$key]['picto'] = get_field('picto', $page['page']);
        }

        $context['menu'] = $this->getMainMenu();
        $context['footer_menu'] = $this->getFooterMenu();

        return $context;
    }

    private function getMainMenu()
    {
        return wp_nav_menu([
            'echo' => false,
            'theme_location' => 'main-nav',
            'container' => 'ul',
            'menu_id' => 'primary-menu',
            'menu_class' => 'nav hidden-menu',
            'link_class' => 'menu--link menu--link__main'
        ]);
    }

    private function getFooterMenu()
    {
        return wp_nav_menu([
            'echo' => false,
            'theme_location' => 'footer-nav',
            'container' => 'ul',
            'menu_id' => 'footer-menu',
            'menu_class' => 'footer-nav',
            'link_class' => 'footer-nav--link'
        ]);
    }
}

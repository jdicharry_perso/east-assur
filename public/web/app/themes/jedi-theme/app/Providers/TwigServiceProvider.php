<?php

namespace App\Providers;

use Rareloop\Lumberjack\Providers\ServiceProvider;
use Rareloop\Lumberjack\Facades\Config;
use manuelodelain\Twig\Extension\SvgExtension;
use App\Lib\Twig\PictoExtension;

class TwigServiceProvider extends ServiceProvider
{
    /**
     * Register any app specific items into the container
     */
    // public function register()
    // {
    // }

    /**
     * Perform any additional boot required for this application
     */
    public function boot()
    {
        add_filter( 'timber/twig', [$this, 'addToTwig'] );
    }

    public function addToTwig($twig)
    {
        $twig->addExtension(new SvgExtension(Config::get('assets.images')));
        $twig->addExtension(new PictoExtension(Config::get('assets.svg')));

        return $twig;
    }
}

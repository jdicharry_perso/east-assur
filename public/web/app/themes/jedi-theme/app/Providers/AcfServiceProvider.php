<?php

namespace App\Providers;

use Rareloop\Lumberjack\Providers\ServiceProvider;
use Rareloop\Lumberjack\Facades\Config;
use App\Lib\Acf\RegisterFields;
use App\Lib\Acf\Search;

class AcfServiceProvider extends ServiceProvider
{
    /**
     * Register any app specific items into the container
     */
    // public function register()
    // {
    // }

    /**
     * Perform any additional boot required for this application
     */
    public function boot()
    {
        new RegisterFields(Config::get('acf'));
        new Search();

        // add_action('acf/init', [$this, 'acf_google_map_api']);
    }

    public function acf_google_map_api()
    {
        // acf_update_setting('google_api_key', Config::get('app.google.map'));
    }
}

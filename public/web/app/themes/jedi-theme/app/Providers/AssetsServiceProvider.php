<?php

namespace App\Providers;

use Rareloop\Lumberjack\Providers\ServiceProvider;
use Rareloop\Lumberjack\Facades\Config;

class AssetsServiceProvider extends ServiceProvider
{
    /**
     * Register any app specific items into the container
     */
    // public function register()
    // {
    // }

    /**
     * Perform any additional boot required for this application
     */
    public function boot()
    {
        // @todo create a specific style for gutenberg
        // add_action( 'after_setup_theme', [$this, 'themeSetup'] );
        add_action('wp_enqueue_scripts', [$this, 'actionEnqueue'], 100);
    }

    public function actionEnqueue()
    {
        wp_enqueue_style('lumberjack/main.css', $this->asset_path('/css/main.css'), false, null);
        wp_enqueue_script('lumberjack/main.js', $this->asset_path('/js/main.js'), ['jquery'], null, true);
    }

    public function themeSetup()
    {
        add_theme_support( 'editor-styles' );
        add_editor_style( $this->asset_path('/css/main.css') );
    }

    private function asset_path($asset)
    {
        $manifestPath = Config::get('assets.manifest');
        $distUri = Config::get('assets.uri');
        $manifest = file_exists($manifestPath) ? json_decode(file_get_contents($manifestPath), true) : [];
        return $distUri . (isset($manifest[$asset]) ? $manifest[$asset] : $asset);
    }
}

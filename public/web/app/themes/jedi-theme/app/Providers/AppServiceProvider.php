<?php

namespace App\Providers;

use Rareloop\Lumberjack\Providers\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any app specific items into the container
     */
    public function register()
    {

    }

    /**
     * Perform any additional boot required for this application
     */
    public function boot()
    {

        /**
        * Allow users with the editor role to use the Caldera Forms admin screen
        */
        add_filter( 'caldera_forms_manage_cap', function( $cap, $context ) {
            if( 'admin' == $context ) {
                return 'edit_pages';
            }

            return $cap;
        }, 10, 2 );

    }


}

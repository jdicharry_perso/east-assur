<?php
/**
 * Template Name: Template Services
 * The template for displaying Service pages.
 *.
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Timber\Timber;

class ServiceTemplateController extends Controller
{
    public function handle()
    {
        $context = Timber::get_context();
        $page = $context['posts'][0];

        $context['post'] = $page;
        $context['picto'] = $page->meta('picto');
        $context['title'] = $page->title;
        $context['content'] = $page->content;
        $context['thumbnail'] = $page->thumbnail;
        $context['service_hero'] = $page->meta('service_hero');
        $context['service_form'] = $page->meta('service_form');
        $context['service_pictos_text'] = $page->meta('service_pictos_text');
        $context['service_text_image_1'] = $page->meta('service_text_image_1');
        $context['service_text_image_2'] = $page->meta('service_text_image_2');
        $context['service_table'] = $page->meta('service_table');




        return new TimberResponse('templates/service-page.twig', $context);
    }
}

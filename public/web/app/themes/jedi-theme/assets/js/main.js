import $ from "jquery";
window.jQuery = $;
window.$ = $;

import "foundation-sites";
import "slick-carousel/slick/slick.min.js";

//** Add Browser alert for IE */

let showBrowserAlert = function() {
    if (document.querySelector(".unsupported-browser")) {
        let d = document.getElementsByClassName("unsupported-browser");

        const is_IE11 = /Trident\/|MSIE/.test(window.navigator.userAgent);

        console.log("is_IE", is_IE11);

        if (is_IE11) {
            d[0].innerHTML =
                '<div class="card--IEBrowser"><div><b class="h2">Ce navigateur est obsolète !</b><p>Ce site web ne fonctionnera pas correctement ici. Merci de télécharger un nouveau navigateur.</p><a href="https://bestvpn.org/outdatedbrowser/fr" target="_blank" class="button primary">En savoir +</a></div></div>';
            d[0].style.display = "block";
        }
    }
};

document.addEventListener("DOMContentLoaded", showBrowserAlert);

// ********* Décalage du corps du site sous le menu général (qui est fixe) *********
resize();

function resize() {
    var headerHeight = $(".header")[0].clientHeight;
    $("main")[0].style.marginTop = headerHeight + "px";
}

// ********* Menu responsive *********

$("#burger-menu").click(function() {
    $(this).toggleClass("is-active");
    $(".nav-primary").toggleClass("show-menu");
});

//**   Menu footer responsive  */

$(".menu-item-has-children>a").append('<span class="toggler">+</span>');

$(".toggler").click(function(e) {
    e.preventDefault();
    $(this)
        .parent()
        .parent()
        .toggleClass("active");
});

//**    Apparition de la navigation bouton Devis en ligne */
$(".js--show-nav").on("click", function() {
    $(".fixed-nav__menu").toggleClass("active");
});

//**    Gestion de la modale (être rappelé) */
$(".js--modal").click(function() {
    $(".modal").toggleClass("active");
});

$(".modal-container").on("click", ".js--close--modal", function() {
    $(".modal").removeClass("active");
});

$(document).on("keydown", function(event) {
    if (event.key == "Escape") {
        $(".modal").removeClass("active");
    }
});

// ********* Apparitions au scroll *********
const mq = window.matchMedia("(max-width: 1024px)");
const sq = window.matchMedia("(max-width: 640px)");
if (sq.matches) {
    var threshold = 0.05;
} else if (mq.matches) {
    var threshold = 0.15;
} else {
    var threshold = 0.15;
}
const options = {
    root: null,
    rootMargin: "0px",
    threshold
};

const handleIntersect = function(entries, observer) {
    entries.forEach(function(entry) {
        if (entry.intersectionRatio > threshold) {
            entry.target.classList.remove("reveal");
            observer.unobserve(entry.target);
        }
    });
};

document.documentElement.classList.add("reveal-loaded");

const observer = new IntersectionObserver(handleIntersect, options);
const targets = document.querySelectorAll(".reveal");
targets.forEach(function(target) {
    observer.observe(target);
});

//** Déroulé des contenus des blocs frontpage Nos formules */
$(".js--see-more").on("click", function() {
    const key = $(this).data("key");
    $(".js--reduced[data-key='" + key + "']").toggleClass("active");
});

//**   Gestion des onglets du tableau en mobile */
$(".js--onglet").on("click", function() {
    const key = $(this).data("key");
    $("td").removeClass("active");
    $(this).addClass("active");
    $("tbody td[data-key='" + key + "']").addClass("active");
});

//**   Initialisation du slider module pictos et textes en mobile only  */

if (mq.matches) {
    $(".mobile-slider").slick({
        arrows: false,
        dots: true,
        mobileFirst: true,
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 1024,
                settings: "unslick"
            }
        ]
    });
}

//** Déroulé de l'accordéon (FAQ) */
$(".js--expand").on("click", function() {
    const $this = $(this);
    $this.toggleClass("active");
    const key = $this.data("key");
    $(".mod--accordion__text[data-key='" + key + "']").toggleClass("active");
});
//** Affichage des éléments de lexique */
$(".js--lexique").on("click", function() {
    const $this = $(this);
    const key = $this.data("key");
    $(".mod--lexique__nav__item").removeClass("active");
    $this.addClass("active");
    $(".mod--lexique__item").removeClass("active");
    $(".mod--lexique__item[data-key='" + key + "']").addClass("active");
});

//** Slider hero front page */

$(".front--hero__slider").slick({
    pauseOnHover: false,
    arrows: false,
    dots: false,
    autoplay: true,
    autoplaySpeed: 3000
});

//VIMEO PLAYER
let section = $("#vimeo-section");
let iframe = $("#vimeo-player");
let player = new Vimeo.Player(iframe);
console.log("player", player);
player.setVolume(0.5);
let togglePause = false;
$(window).scroll(function() {
    if (section) {
        if (
            $(window).scrollTop() >=
                section.offset().top - $(window).height() / 2 &&
            $(window).scrollTop() <=
                section.offset().top + $(window).height() / 3
        ) {
            if (!togglePause) {
                player.play();
            }
            console.log("section");
            togglePause = true;
        } else {
            player.pause();
            togglePause = false;
        }
    }
});
